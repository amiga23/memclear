# memclear
Based on the work of Dallas J Hodgson which can be found on Aminet  
http://aminet.net/package/dev/debug/MemClear

## Introduction
This tool overwrites the free memory either with zeros or with a value given as parameter. Also it prints out how much memory was cleared and how many chunks exist (fragmentation).

This new version also prints out all the memory regions with their start and end addresses. Additionally it does a check if the memory got corrupted, be comparing how much should be free and how much memory got cleared.

## Usage
Simply run it from Shell or Workbench. Optionally give a Hex value from 0 (default) to FF to chnage the pattern, the empty memory gets overwritten wirth.

## Output
```
MemClear v1.50 : © 2019 Thomas Scheller
                 © 1986-90, Dallas J. Hodgson
Range                 Size      Free      Flags
 16777248 - 134217728 117440480  73341248 FAST PUBLIC LOCAL KICK OK
134217760 - 536870912 402653152 402653152 FAST PUBLIC LOCAL KICK OK
 12582944 -  14155776   1572832   1572072 FAST PUBLIC LOCAL 24bitDMA KICK OK
    16416 -   2097152   2080736   1779992 CHIP PUBLIC LOCAL 24bitDMA KICK ERR 176 -1779816
  2097184 -  12058624   9961440   9874072 CHIP PUBLIC LOCAL 24bitDMA KICK OK
  9874128 bytes of CHIP memory in 15 MemChunks filled w/value 0H.
477560272 bytes of FAST memory in 775 MemChunks filled w/value 0H.
```
This example output shows 5 memory regions. The fourth one got corrupted. Only 176 Bytes have been zeroed, but it should have been 1779992 Bytes.

## Developer information
The original version by Dallas J Hodgson was copiled with Aztec C. For this version SAS/C was used.